package dao.data;

//@Entity
//@Table(name = "pOutStreamOutXML_crd_ys")
public class FileDataOut {
    //private int id;
    //private int spid;
    private String fileName;
    private String fileTextXML;

    public String getFileName() {
        return fileName;
    }

    public FileDataOut setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public String getFileTextXML() {
        return fileTextXML;
    }

    public FileDataOut setFileTextXML(String fileTextXML) {
        this.fileTextXML = fileTextXML;
        return this;
    }

    /*
    @Id
    @Column(name = "Id", nullable = false, insertable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.AUTO) //, generator="native")
    //@GenericGenerator(name = "native", strategy = "native")
    public int getId() {
        return id;
    }

    @Column(name = "SPID", nullable = false, insertable = true, updatable = true)
    public int getSpid() {
        return spid;
    }

    @Column(name = "FileName", nullable = false, insertable = true, updatable = true)
    public String getFileName() {
        return fileName;
    }

    @Column(name = "FileTextXML", nullable = false, insertable = true, updatable = true)
    public String getFileTextXML() {
        return fileTextXML;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setFileTextXML(String fileTextXML) {
        this.fileTextXML = fileTextXML;
    }

    public void setSpid(int spid) {
        this.spid = spid;
    }


    @Override
    public String toString() {
        return "FileDataOut{" +
                "fileName='" + fileName + '\'' +
                ", fileTextXML='" + fileTextXML + '\'' +
                '}';
    }*/
}

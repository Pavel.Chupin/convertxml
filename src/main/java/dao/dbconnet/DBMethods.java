package dao.dbconnet;

import dao.data.FileDataOut;
import dao.dbconnet.impl.DBMethodsImpl;
import org.hibernate.Session;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DBMethods implements DBMethodsImpl {
    private Session session;
    private int spid;

    public DBMethods()  {
        session = HibernateSessionFactory.getSessionFactory().openSession();
        List<Object> spids = session.createSQLQuery("select @@SPID as SPID").list();
        this.spid = Integer.parseInt(spids.get(0).toString());
    }

    public void sessionClose()  {
        session.close();
    }

    public void beginTran(){
        session.beginTransaction();
    }

    public void commitTran(){
        session.getTransaction().commit();
    }

    public void rollbackTran(){
        session.getTransaction().rollback();
    }

    //Очистить входную таблицу и получить текущий СПИД
    public void deleteTableIn() throws SQLException {
        session.getNamedQuery("CRD_DeleteTableIN").setParameter("spid", getSpid()).executeUpdate();
    }

    //Заполнить входную таблицу
    public void insertTableIn(String fileName, String fileText)  throws SQLException{
        /*FileDataIn fileDataIn = new FileDataIn();
        fileDataIn.setFileName(fileName);
        fileDataIn.setSpid(this.spid);
        fileDataIn.setFileTextXML(fileText);
        session.save(fileDataIn);*/
        session.getNamedQuery("CRD_InsertToTableIN")
                .setParameter("spid", getSpid())
                .setParameter("filetextxml",fileText)
                .setParameter("filename",fileName).executeUpdate();
    }

    public /*List<FileDataOut>*/ List<FileDataOut> selectTableOut() throws SQLException{
        List<FileDataOut> fileDataOuts = new ArrayList<FileDataOut>();
        //List<FileDataOut> fileDataOuts = session.createQuery("from FileDataOut where spid = :SPID").setParameter("SPID", getSpid()).list();
        //List<FileDataOut> fileDataOuts = session.createSQLQuery("select * from pOutStreamOutXML_crd_ys with (nolock) where spid = :spid").setParameter("spid", getSpid()).list();
        List<Object[]> list = session.getNamedQuery("CRD_SelectTableOUT").setParameter("spid", getSpid()).list();
        for (Object[] p: list) {
            fileDataOuts.add(new FileDataOut().setFileName((String) p[0]).setFileTextXML((String) p[1]));
        }
        //return fileDataOuts;
        return fileDataOuts;
    }

    public int getSpid() {
        return spid;
    }

    public void deleteTableOut() throws SQLException {
        session.getNamedQuery("CRD_DeleteTableOUT").setParameter("spid", getSpid()).executeUpdate();
    }

    public void execProcedureConvertXML() throws SQLException{
        /*int exRow = *///session.createSQLQuery("{CALL CRD_ParseOutStream(:spid)}").setParameter("spid", getSpid()).executeUpdate();
        //System.out.println("Executed Rows from Stored Procedure****************"+exRow);
        session.getNamedQuery("CRD_Parse").setParameter("spid", getSpid()).executeUpdate();
    }
}

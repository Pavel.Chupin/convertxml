package dao.dbconnet.impl;

import dao.data.FileDataOut;

import java.sql.SQLException;
import java.util.List;

public interface DBMethodsImpl {

    public void insertTableIn(String fileName, String fileText) throws SQLException;

    public List<FileDataOut> selectTableOut() throws SQLException;

    public void execProcedureConvertXML() throws SQLException;

    public void sessionClose();

    public void deleteTableIn() throws SQLException;

    public void deleteTableOut() throws SQLException;

    void beginTran();

    void commitTran();

    void rollbackTran();
}

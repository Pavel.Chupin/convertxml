package main.data;

import java.io.File;

public class DataFile {
    private File file;
    private String outPath;

    public DataFile(File file, String outPath){
        this.file = file;
        this.outPath = outPath;
    }
    public File getFile() {
        return file;
    }

    public String getOutPath() {
        return outPath;
    }
}

package main;


import main.data.DataFile;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileOperation {

    public static List<File> getListFileFromCatalog(String file_path) {
        List<File> files = new ArrayList<>();
        File dir = new File(file_path); //path указывает на директорию
        File[] arrFiles = dir.listFiles();

        for (File file: Arrays.asList(arrFiles)) {
            if (!file.getName().substring(0,3).toUpperCase().equals("MSG")){
                files.add(file);
            }
        }
        //files = Arrays.asList(arrFiles);

        return files /*files*/;
    }

    public static String readFile(File file, String encoding) throws IOException {
        FileInputStream fis = null;
        BufferedReader br = null;
        String fileText = "";

        try {
            //fis =  new FileInputStream(this.filePathIn + "/" + fileName);
            fis = new FileInputStream(file);
            br = new BufferedReader(new InputStreamReader(fis, encoding));
            String buf;
            while ((buf = br.readLine()) != null) {
                fileText = fileText + buf + "\r\n";
            }
        } catch (IOException e) {
            throw e;
        } finally {
            br.close();
            fis.close();
        }
        return fileText;
    }

    public static void writeFile(String absoluteFilePath, String text, String encoding) throws IOException {
        PrintWriter writeFile = null;
        try {
            writeFile = new PrintWriter(absoluteFilePath, encoding);
            writeFile.println(text);
        } catch (IOException e) {
            throw e;
        } finally {
            writeFile.close();
        }
    }

    public static void copyFile(List<DataFile> dataFiles) throws IOException {
        for (DataFile dataFile : dataFiles) {
            Files.copy(dataFile.getFile().toPath(), new File(dataFile.getOutPath() + "/" + dataFile.getFile().getName()).toPath());
            /*
            //Файлы для исходящего потока
            if (file.getPath().equals(file_path_blaster_out)) {
                Files.copy(file.toPath(), new File(file_path_utilite_out + "/" + file.getName()).toPath());
            }
            //Файлы для входящего потока
            else if (file.getPath().equals(file_path_utilite_in)) {
                Files.copy(file.toPath(), new File(file_path_blaster_in + "/" + file.getName()).toPath());
            }*/
        }
    }

    public static void deleteFile(List<DataFile> dataFiles) {
        for (DataFile dataFile : dataFiles) {
            dataFile.getFile().delete();
        }
    }
}


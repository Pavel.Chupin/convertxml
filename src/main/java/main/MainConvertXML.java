package main;


import dao.data.FileDataOut;
import dao.dbconnet.DBMethods;
import dao.dbconnet.impl.DBMethodsImpl;
import main.data.DataFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class MainConvertXML {
    public static void main(String[] args) {

        FileInputStream fis;
        Properties property = new Properties();

        //Инициирцем базу
        DBMethodsImpl dbMethods = new DBMethods();

        try {
            //fis = new FileInputStream(args[0]);
            //fis = new FileInputStream("src/main/resources/config.properties");
            fis = new FileInputStream("config.properties");
            property.load(fis);

            String file_path_blaster_out = property.getProperty("file_path_blaster_out");
            String file_path_utilite_out = property.getProperty("file_path_utilite_out");
            String file_path_utilite_in = property.getProperty("file_path_utilite_in");
            String file_path_blaster_in = property.getProperty("file_path_blaster_in");

            List<String> filesPath = new ArrayList<>();
            filesPath.add(file_path_blaster_out);
            filesPath.add(file_path_utilite_in);

            String file_encoding_in = property.getProperty("file_encoding_in");
            String file_encoding_out = property.getProperty("file_encoding_out");
            String file_header_in = property.getProperty("file_header_in");
            //String file_header_out = property.getProperty("file_header_out");
            String file_header_encoding_out = property.getProperty("file_header_encoding_out");
            int time = Integer.parseInt(property.getProperty("time_sleep_second").trim());

            //Запускаем цикл опроса папки
            while (true) {
                //Период ожидания
                TimeUnit.SECONDS.sleep(time);

                //Получаем список файлов из каталогов
                List<DataFile> dataFiles = new ArrayList<>();
                for (String path : filesPath) {
                    for (File file : FileOperation.getListFileFromCatalog(path)) {
                        dataFiles.add(new DataFile(file, path.equals(file_path_blaster_out) ? file_path_utilite_out : file_path_blaster_in));
                    }
                }

                if (dataFiles != null && dataFiles.size() > 0) {
                    dbMethods.beginTran();
                    dbMethods.deleteTableIn();

                    List<DataFile> copyDataFiles = new ArrayList<>();
                    for (DataFile dataFile : dataFiles) {
                        if ((dataFile.getOutPath().equals(file_path_utilite_out) && dataFile.getFile().getName().substring(0, 7).toUpperCase().equals("XADVAPL"))
                                || (dataFile.getOutPath().equals(file_path_blaster_in) && dataFile.getFile().getName().substring(0, 8).toUpperCase().equals("RXADVAPL"))
                                || (dataFile.getOutPath().equals(file_path_blaster_in) && dataFile.getFile().getName().substring(0, 1).toUpperCase().equals("G"))
                                || (dataFile.getOutPath().equals(file_path_blaster_in) && dataFile.getFile().getName().substring(0, 1).toUpperCase().equals("M"))
                                ) {
                            String text = FileOperation.readFile(dataFile.getFile(), file_encoding_in).replace(file_header_in, "");
                            dbMethods.insertTableIn(dataFile.getFile().getName(), text);
                        } else {
                            copyDataFiles.add(dataFile);
                        }
                    }

                    dbMethods.commitTran();

                    dbMethods.beginTran();
                    dbMethods.execProcedureConvertXML();
                    dbMethods.commitTran();

                    List<FileDataOut> fileDataOuts;
                    fileDataOuts = dbMethods.selectTableOut();

                    dbMethods.beginTran();
                    dbMethods.deleteTableOut();
                    dbMethods.commitTran();

                    // Сохраняем файлы на диск
                    for (FileDataOut f : fileDataOuts) {
                        //Если файл не XML, то запускаем без трансформации
                        if (f.getFileName().substring(0, 7).toUpperCase().equals("XADVAPL")
                                || f.getFileName().substring(0, 8).toUpperCase().equals("RXADVAPL")
                                ) {
                            String path = f.getFileName().substring(0, 1).toUpperCase().equals("X") ? file_path_utilite_out : file_path_blaster_in;

                            FileOperation.writeFile(path + "/" + f.getFileName()
                                    , XmlFormatter.prettyFormat(f.getFileTextXML(), "2", file_header_encoding_out)
                                    , file_encoding_out);
                        } else {
                            FileOperation.writeFile(file_path_blaster_in + "/" + f.getFileName(), f.getFileTextXML(), file_encoding_out);
                        }
                    }

                    //Файлы которые не нужно пересобирать просто копируем
                    FileOperation.copyFile(copyDataFiles);
                    //Удаляем все файлы
                    FileOperation.deleteFile(dataFiles);

                    //Почистим память
                    fileDataOuts.clear();
                    copyDataFiles.clear();
                    dataFiles.clear();
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbMethods.sessionClose();
        }

    }
}
